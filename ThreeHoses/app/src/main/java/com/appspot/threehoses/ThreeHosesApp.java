package com.appspot.threehoses;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class ThreeHosesApp extends Application {
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;

    @Override
    public void onCreate(){
        super.onCreate();
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sp.edit();
    }

    public synchronized void savePrefs(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public synchronized String getStringPref(String key) {
        return sp.getString(key, null);
    }

    public synchronized void removePref(String key) {
        editor.remove(key);
        editor.commit();
    }

    public synchronized boolean hasPref(String key) {
        return sp.contains(key);
    }

    public String getVersion(){
        String version = "0.0";
        try {
            version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }

    public void showMessage(String title, String msg, Activity a){
        showMessage(title, msg, a, false);
    }

    public void showMessage(String title, String msg, Activity a, boolean close){
        final boolean shouldClose = close;
        final Activity fa = a;
        final AlertDialog.Builder builder = new AlertDialog.Builder(a);
        builder.setMessage(msg)
                .setTitle(title)
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        if(shouldClose){
                            fa.finish();
                        }
                    }
                });
        AlertDialog d = builder.create();
        d.show();
    }
}
