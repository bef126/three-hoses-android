package com.appspot.threehoses;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.http.AndroidHttpClient;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class HttpUtil {
    private static final String TAG = "HttpUtil";
    public final Object httpLock = new Object();
    private ThreeHosesApp app;

    public HttpUtil(ThreeHosesApp a) {
        this.app = a;
    }

    public boolean isOnline() {
        try {
            ConnectivityManager connMgr = (ConnectivityManager) app.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (Connectivity.isConnectedWifi(app)) {
                WifiManager wifiMgr = (WifiManager) app.getSystemService(Context.WIFI_SERVICE);
                WifiInfo wi = wifiMgr.getConnectionInfo();
                // only return true if all authentication is completed
                return (wi.getSupplicantState() == SupplicantState.COMPLETED);
            } else {
                return (networkInfo != null && networkInfo.isConnected());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public JSONObject doVerb(String verb, String path, MyHttpParams params, boolean firstTime) throws Exception {
        return doVerbArray(verb, path, params, firstTime).getJSONObject(0);
    }

    public JSONArray doVerbArray(String verb, String path, MyHttpParams params, boolean firstTime) throws Exception {
        synchronized (httpLock) {
            Log.d(TAG, "do" + verb + " for " + Constants.HOST + path + " with " + params.toString());
            if (isOnline()) {
                AndroidHttpClient httpClient = AndroidHttpClient.newInstance(Constants.USER_AGENT + " - " + app.getVersion());
                HttpResponse httpResponse;
                try {
                    if (verb.equals("POST")) {
                        HttpPost request = new HttpPost(Constants.HOST + path);
                        request.setEntity(params.forPost());
                        httpResponse = httpClient.execute(request);
                    } else if (verb.equals("GET")) {
                        String uri = Constants.HOST + path + "?" + params.toString();
                        HttpGet request = new HttpGet(uri);
                        httpResponse = httpClient.execute(request);
                    } else if (verb.equals("PUT")) {
                        HttpPut request = new HttpPut(Constants.HOST + path);
                        request.setEntity(params.forPost());
                        httpResponse = httpClient.execute(request);
                    } else if (verb.equals("DELETE")) {
                        String uri = Constants.HOST + path + "?" + params.toString();
                        HttpDelete request = new HttpDelete(uri);
                        httpResponse = httpClient.execute(request);
                    }
                    // default to GET
                    else {
                        String uri = Constants.HOST + path + "?" + params.toString();
                        HttpGet request = new HttpGet(uri);
                        httpResponse = httpClient.execute(request);
                    }
                    int status_code = httpResponse.getStatusLine().getStatusCode();
                    Log.d(TAG, "status_code" + status_code);
                    Log.d(TAG, "statusline" + httpResponse.getStatusLine().toString());
                    InputStream inputStream;
                    inputStream = httpResponse.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk;
                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    String result = stringBuilder.toString();
                    Log.d(TAG, "do" + verb + " got result: " + result);
                    if (status_code == 200 || status_code == 201) {
                        JSONArray jObject;
                        try{
                            jObject = new JSONArray(result);
                        } catch(Exception NotAnArray){
                            JSONObject j = new JSONObject(result);
                            jObject = new JSONArray();
                            jObject.put(0, j);
                        }
                        Log.d(TAG, "parsed json as: " + jObject);
                        return jObject;
                    } else if (result.equals("Session Expired")){
                        // TODO - figure out how to handle expired sessions
                        //
                        // my first thought was to obtain a new session token, then do something like:
                        //
                        // return doVerb(verb, path, params, false);
                        //
                        // however, I'm not sure if I want to allow someone to be logged in forever
                        // for now, I'll just re-throw the exception
                        throw new Exception(result);
                    } else {
                        throw new Exception(result);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    throw e;
                } finally {
                    httpClient.close();
                }
            } else {
                throw new Exception("No Internet Detected");
            }
        }
    }

    ////////////////////
    // single objects //
    ////////////////////
    public JSONObject doPost(String path, MyHttpParams params) throws Exception {
        return doPost(path, params, true);
    }

    public JSONObject doPost(String path, MyHttpParams params, boolean firstTime) throws Exception {
        return doVerb("POST", path, params, firstTime);
    }

    public JSONObject doGet(String path, MyHttpParams params) throws Exception {
        return doGet(path, params, true);
    }

    public JSONObject doGet(String path, MyHttpParams params, boolean firstTime) throws Exception {
        return doVerb("GET", path, params, firstTime);
    }

    ////////////
    // arrays //
    ////////////
    public JSONArray doPostArray(String path, MyHttpParams params) throws Exception {
        return doPostArray(path, params, true);
    }

    public JSONArray doPostArray(String path, MyHttpParams params, boolean firstTime) throws Exception {
        return doVerbArray("POST", path, params, firstTime);
    }

    public JSONArray doGetArray(String path, MyHttpParams params) throws Exception {
        return doGetArray(path, params, true);
    }

    public JSONArray doGetArray(String path, MyHttpParams params, boolean firstTime) throws Exception {
        return doVerbArray("GET", path, params, firstTime);
    }

}
