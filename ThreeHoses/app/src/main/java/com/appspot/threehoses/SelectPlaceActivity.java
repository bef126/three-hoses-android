package com.appspot.threehoses;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SelectPlaceActivity extends ActionBarActivity {

    private double lat;
    private double lng;
    private ThreeHosesApp app;
    private Api api;
    private List<String> locations;
    private List<Map<String,String>> locationData;
    private ListView lv;
    private static final String TAG = "SelectPlaceActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_place);

        app = (ThreeHosesApp) this.getApplicationContext();
        api = new Api(app);

        lat = getIntent().getDoubleExtra("lat", 0.0);
        lng = getIntent().getDoubleExtra("lng", 0.0);

        lv = (ListView) findViewById(R.id.listView);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Map<String,String> item = locationData.get(position);
                String name = item.get("name");
                String gid = item.get("id");
                double lat = Double.parseDouble(item.get("lat"));
                double lng = Double.parseDouble(item.get("lng"));
                returnResults(name, gid, lat, lng);
            }

        });

        locations = new ArrayList<String>();
        locationData = new ArrayList<Map<String,String>>();

        getNearbyPlaces();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_select_place, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getNearbyPlaces() {
        final Context me = this;
        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                progressDialog = ProgressDialog.show(me, "", getString(R.string.please_wait), false);
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... p) {
                try {
                    JSONArray a = api.lookupNearby(lat, lng);
                    for (int i = 0; i < a.length(); i++) {
                        JSONObject o = a.getJSONObject(i);
                        String name = o.getString("name");
                        String id = o.getString("place_id");
                        JSONObject o2 = o.getJSONObject("geometry");
                        JSONObject o3 = o2.getJSONObject("location");
                        lat = o3.getDouble("lat");
                        lng = o3.getDouble("lng");
                        locations.add(name);
                        Map<String,String> map = new HashMap<String,String>();
                        map.put("name", name);
                        map.put("id", id);
                        map.put("lat", ""+lat);
                        map.put("lng", ""+lng);
                        locationData.add(map);
                    }
                    return null;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                progressDialog.dismiss();
                if(locations.size() > 0) {
                    lv.setAdapter(new ArrayAdapter<String>(me, android.R.layout.simple_list_item_1, locations));
                } else {
                    alertNoResults();
                }
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

    private void returnResults(String name, String gid, double lat, double lng){
        Intent returnIntent = getIntent();
        returnIntent.putExtra("name", name);
        returnIntent.putExtra("id", gid);
        returnIntent.putExtra("lat", lat);
        returnIntent.putExtra("lng", lng);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    private void returnNoResults(){
        Intent returnIntent = getIntent();
        setResult(RESULT_CANCELED, returnIntent);
        finish();
    }

    public void alertNoResults(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Could not find any gas stations at your current location")
                .setTitle("No Results")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        returnNoResults();
                    }
                });
        AlertDialog d = builder.create();
        d.show();
    }
}
