package com.appspot.threehoses;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends ActionBarActivity {

    private LocationUtil lu;
    private double lat;
    private double lng;
    public static final int SELECT_PLACE_ACTIVITY = 1;
    public static final int CHOOSE_NEARBY_ACTIVITY = 2;
    private ThreeHosesApp app;
    private Api api;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        app = (ThreeHosesApp) this.getApplicationContext();
        api = new Api(app);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            app.showMessage(getString(R.string.app_name), "Version - " + app.getVersion(), this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_PLACE_ACTIVITY) {
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra("name");
                String id = data.getStringExtra("id");
                double lat = data.getDoubleExtra("lat", 0.0);
                double lng = data.getDoubleExtra("lng", 0.0);
                confirmNumberOfHoses(name, id, lat, lng);
            }
        } else if (requestCode == CHOOSE_NEARBY_ACTIVITY) {
            if (resultCode == RESULT_OK) {
                double dlat = data.getDoubleExtra("lat", 0.0);
                double dlng = data.getDoubleExtra("lng", 0.0);
                showDirections(dlat, dlng);
            }
        }
    }

    private void startLocationUpdates(){
        // ensure we dispose of any prior location listeners before we start new ones
        stopLocationUpdates();

        lu = new LocationUtil(1, this);
    }

    private void stopLocationUpdates(){
        try{
            lu.shutdown();
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    private void setCurrentLocation(){
        Location l = lu.currentBestLocation();
        lat = l.getLatitude();
        lng = l.getLongitude();
    }

    public void addOrConfirm(View view) {
        waitForLocation(SELECT_PLACE_ACTIVITY);
    }

    public void findNearby(View view) {
        waitForLocation(CHOOSE_NEARBY_ACTIVITY);
    }

    private void createLocation(final String name, final String id, final double lat, final double lng, final int hoses) {
        final Context me = this;
        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                progressDialog = ProgressDialog.show(me, "", getString(R.string.please_wait), false);
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... p) {
                try {
                    api.create(lat, lng, name, id, hoses);
                    return null;
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                progressDialog.dismiss();
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

    private void showDirections(double dlat, double dlng){
        Intent intent = new Intent(Intent.ACTION_VIEW,
            Uri.parse("http://maps.google.com/maps?" +"saddr=" + dlat + "," + dlng)
        );
        intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
        startActivity(intent);

    }

    private void confirmNumberOfHoses(final String name, final String gid, final double lat, final double lng) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Number of Hoses");
        alertDialogBuilder
                .setMessage("How many hoses does this station have?")
                .setCancelable(true)
                .setPositiveButton("One",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                        createLocation(name, gid, lat, lng, 1);
                    }
                })
                .setNegativeButton("Three",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                        createLocation(name, gid, lat, lng, 3);
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void waitForLocation(final int destination) {
        final Context me = this;
        startLocationUpdates();
        class SendPostReqAsyncTask extends AsyncTask<String, Void, Boolean> {
            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                progressDialog = ProgressDialog.show(me, "", "Getting your location", false);
                super.onPreExecute();
            }

            @Override
            protected Boolean doInBackground(String... p) {
                int timeoutInSeconds = 20;
                while(timeoutInSeconds > 0 && lu.hasAcceptableLocation() == false){
                    try{
                        Thread.sleep(1000);
                    } catch(Exception e){
                        e.printStackTrace();
                    }
                    timeoutInSeconds--;
                }
                return lu.hasAcceptableLocation();
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                progressDialog.dismiss();
                if(result){
                    stopLocationUpdates();
                    if(destination == SELECT_PLACE_ACTIVITY){
                        setCurrentLocation();
                        Intent i = new Intent(me, SelectPlaceActivity.class);
                        i.putExtra("lat", lat);
                        i.putExtra("lng", lng);
                        startActivityForResult(i, SELECT_PLACE_ACTIVITY);
                    } else if(destination == CHOOSE_NEARBY_ACTIVITY){
                        setCurrentLocation();
                        Intent i = new Intent(me, ChooseNearbyActivity.class);
                        i.putExtra("lat", lat);
                        i.putExtra("lng", lng);
                        startActivityForResult(i, CHOOSE_NEARBY_ACTIVITY);
                    }
                } else {
                    noLocation();
                }
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

    private void noLocation() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Location Error");
        alertDialogBuilder
                .setMessage("Unable to determine your location. Please make sure you have GPS turned on and try again.")
                .setCancelable(true)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
