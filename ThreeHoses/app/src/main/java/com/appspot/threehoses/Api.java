package com.appspot.threehoses;

import org.json.JSONArray;
import org.json.JSONObject;

public class Api {
    private HttpUtil http;
    private ThreeHosesApp app;

    public Api(ThreeHosesApp a) {
        this.http = new HttpUtil(a);
        this.app = a;
    }

    public JSONObject create(double lat, double lng, String name, String id, int hoses) throws Exception {
        MyHttpParams params = new MyHttpParams();
        params.add("name", name);
        params.add("lat", ""+lat);
        params.add("lng", ""+lng);
        params.add("id", id);
        params.add("hoses", ""+hoses);
        JSONObject j = http.doPost("/v1/create", params);

        return j;
    }

    public JSONArray lookupNearby(double lat, double lng) throws Exception {
        MyHttpParams params = new MyHttpParams();
        params.add("lat", ""+lat);
        params.add("lng", ""+lng);
        JSONArray a = http.doPostArray("/v1/lookup", params);

        return a;
    }

    public JSONArray findClosest(double lat, double lng) throws Exception {
        MyHttpParams params = new MyHttpParams();
        params.add("lat", ""+lat);
        params.add("lng", ""+lng);
        JSONArray a = http.doPostArray("/v1/search", params);

        return a;
    }
}
