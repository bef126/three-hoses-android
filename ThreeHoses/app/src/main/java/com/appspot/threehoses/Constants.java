package com.appspot.threehoses;

public class Constants {
    public static final String USER_AGENT = "Three-Hoses Android";
    public static final String HOST = "https://three-hoses.appspot.com";
    //public static final String HOST = "http://192.168.1.64:8080";
}
