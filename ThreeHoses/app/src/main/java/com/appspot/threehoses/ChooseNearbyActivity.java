package com.appspot.threehoses;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChooseNearbyActivity extends ActionBarActivity {

    private double lat;
    private double lng;
    private ThreeHosesApp app;
    private Api api;
    private ListView lv;
    private static final String TAG = "ChooseNearbyActivity";
    private List<Map<String,String>> locations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_nearby);

        app = (ThreeHosesApp) this.getApplicationContext();
        api = new Api(app);

        lat = getIntent().getDoubleExtra("lat", 0.0);
        lng = getIntent().getDoubleExtra("lng", 0.0);

        lv = (ListView) findViewById(R.id.listView);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Map<String,String> item = locations.get(position);
                double lat = Double.parseDouble(item.get("lat"));
                double lng = Double.parseDouble(item.get("lng"));
                returnResults(lat, lng);
            }

        });

        locations = new ArrayList<Map<String,String>>();

        getNearbyStations();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_choose_nearby, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getNearbyStations() {
        final Context me = this;
        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                progressDialog = ProgressDialog.show(me, "", getString(R.string.please_wait), false);
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... p) {
                try {
                    JSONArray a = api.findClosest(lat, lng);
                    for (int i = 0; i < a.length(); i++) {
                        JSONObject o = a.getJSONObject(i);
                        Map<String,String> map = new HashMap<String,String>();

                        String n = o.getString("Name");
                        if(n.length() < 1){
                            n = "No Name";
                        }
                        map.put("name", n);
                        double distance = o.getDouble("DistanceInMiles");
                        NumberFormat formatter = new DecimalFormat("#0.0");
                        map.put("distance", formatter.format(distance) + " miles");
                        JSONObject o2 = o.getJSONObject("Location");
                        map.put("lat", ""+o2.getDouble("Lat"));
                        map.put("lng", ""+o2.getDouble("Lng"));
                        locations.add(map);
                    }
                    return null;
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                progressDialog.dismiss();
                if(locations.size() > 0) {
                    ArrayAdapter itemsAdapter = new ArrayAdapter<Map<String,String>> (me, android.R.layout.simple_list_item_2, android.R.id.text1, locations)
                    {
                        public View getView(int position, View convertView, ViewGroup parent) {
                            View view = super.getView(position, convertView, parent);
                            TextView title = (TextView) view.findViewById(android.R.id.text1);
                            TextView subtitle = (TextView) view.findViewById(android.R.id.text2);
                            Map<String,String> item = locations.get(position);
                            title.setText(item.get("name"));
                            subtitle.setText(item.get("distance"));
                            return view;
                        }
                    };
                    lv.setAdapter(itemsAdapter);
                } else {
                    alertNoResults();
                }
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

    private void returnResults(double lat, double lng){
        Intent returnIntent = getIntent();
        returnIntent.putExtra("lat", lat);
        returnIntent.putExtra("lng", lng);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    private void returnNoResults(){
        Intent returnIntent = getIntent();
        setResult(RESULT_CANCELED, returnIntent);
        finish();
    }

    public void alertNoResults(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Could not find any nearby gas stations")
                .setTitle("No Results")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        returnNoResults();
                    }
                });
        AlertDialog d = builder.create();
        d.show();
    }
}
